﻿# Prueba técnica - Prisa

## Endpoints

-   getcustomer
-   getcustomer/rut
-   setcustomer
-   updatecustomer/rut

_El RUT debe ir con puntos y guion (XX.XXX.XXX-X)._  
_./database contiene el script para crear y poblar la base de datos._

Esta API se puede probar en https://prueba-tecnica-prisa.herokuapp.com/