const express = require("express");
const app = express();

// middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// routes
app.use(require("./routes/router"));

app.listen(3000);
console.log("[Escuchando en el puerto 3000]");