const { Pool } = require("pg");

// conexión a db
const pool = new Pool({
    host: "localhost",
    user: "postgres",
    password: "password",
    database: "prisa_customers",
    port: "5432",
});

// getcustomer
const getCustomer = async(req, res) => {
    try {
        const response = await pool.query("SELECT * FROM customer");
        res.json(response.rows);
    } catch (error) {
        res.status(500).send({
            error: error.detail,
        });

        console.error(error);
    }
};

// getcustomer/rut
const getCustomerByRut = async(req, res) => {
    const rut = req.params.rut;

    try {
        const response = await pool.query("SELECT * FROM customer WHERE rut = $1", [rut]);

        // se valida si el rut ingresado devuelve algún objeto
        if (response.rowCount == 0) {
            res.status(404).send({
                error: "No existe ningún cliente con el RUT ingresado.",
                attempted_rut: rut,
            });

            console.log(`No existe ningún cliente con RUT ${rut}.`);
        } else {
            res.json(response.rows);
        }
    } catch (error) {
        res.status(500).send({
            error: error.detail,
        });

        console.error(error);
    }
};

// setcustomer
const setCustomer = async(req, res) => {
    const { rut, name, last_name, address, email, is_active, debt } = req.body;

    try {
        const response = await pool.query(
            "INSERT INTO customer (rut, name, last_name, address, email, is_active, debt) VALUES ($1, $2, $3, $4, $5, $6, $7)", [rut, name, last_name, address, email, is_active, debt]
        );

        res.json({
            message: "Cliente agregado exitosamente.",
            body: {
                new_customer: {
                    rut,
                    name,
                    last_name,
                    address,
                    email,
                    is_active,
                    debt,
                },
            },
        });

        console.log(response);
    } catch (error) {
        res.status(500).send({
            error: error.detail,
        });

        console.error(error);
    }
};

// updatecustomer/rut
const updateCustomer = async(req, res) => {
    const rut = req.params.rut;
    const { email, is_active } = req.body;

    try {
        const response = await pool.query("UPDATE customer SET email = $1, is_active = $2 WHERE rut = $3", [
            email,
            is_active,
            rut,
        ]);

        // se valida si el rut ingresado devuelve algún objeto
        if (response.rowCount == 0) {
            res.status(404).send({
                error: "No existe ningún cliente con el RUT ingresado.",
                attempted_rut: rut,
            });

            console.log(`No existe ningún cliente con RUT ${rut}.`);
        } else {
            res.json({
                message: "Cliente actualizado.",
                body: {
                    updated_customer: rut,
                    new_data: {
                        email,
                        is_active,
                    },
                },
            });

            console.log(response);
        }
    } catch (error) {
        res.status(500).send({
            error: error.detail,
        });

        console.error(error);
    }
};

module.exports = {
    getCustomer,
    getCustomerByRut,
    setCustomer,
    updateCustomer,
};